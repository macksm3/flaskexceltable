# FlaskExcelTable-m3fork


## Name
FlaskExcelTable

## Description
Table Display of Links from Python Beginners and Intermediate Jeff.Pro Telegram Channels.
#
## Usage
Search for any keyword. Toggle through the found matches. The URLs are not hyperlinks, but copy URL and paste 
into your browser works! One column identifies Class videos. Other entries are from class participants. 
PostDate is the approximate date posted into our chats. Replit assignments refer to my posts, which may not all
work for everyone-- at least they are listed as a reminder of what assignments were done!

## Roadmap
I will continue to update with new links as we continue through our Intermediate Python course.

## Contributing
Formatting is minimal due to the minimal options available when you use this method: create Excel sheet, 
have pandas read the sheet and return the dataframe in pandas version of html. 

I found I could make the column labels bold. I could center the column labels and I could format the dates,
but these formatting options were WITHIN the pd.read_excel() options, NOT in html. I think this is as "pretty" 
as I can make it. (The date columns/headers look best in LibreWolf/Firefox rather than ungoogled-chromium or Web.) 

In order to gain much more flexibility in formatting html/css on this web page, I plan to convert the Excel 
worksheet into an SQLite database and begin working with it from there. I will post another version of the 
project when I get that step completed.

I will be happy to provide the original Excel workbook (one sheet is 'for_pycharm'), if anyone is interested.

## Authors and acknowledgment
I created this for my own use, but thought it might be appreciated by others as a source document.

## License
Open source, for use by students in our classes. I have not created a license yet.

## Project status
I will continue to add links to the end of our Intermediate Python course.
