from flask import Flask, render_template
import pandas as pd


app = Flask(__name__)

pd.set_option('display.width', 1000)
pd.set_option('colheader_justify', 'center')


@app.route("/")
def index():

    with open("links.xlsx"):
        df = pd.read_excel('links.xlsx', dtype={
            'postdate': 'datetime64', 'created': 'datetime64'}, index_col=None, header=0)
        df = df.fillna("")
        # return df.to_html(bold_rows=True)
        content = df.to_html(bold_rows=True)
        return render_template('main.html', content=content)


if __name__ == "__main__":
    app.run(debug=True)
